Auto Assign Role
================================================================================

This module serves two purposes.
  1) Automatically assign a new user to a specific role when created via the
     admin interface or from a new user registration.
  2) Allow a new user registering on the site to select designated roles they
     would like to belong to.

How to use
--------------------------------------------------------------------------------

1. Activate the Module
3. Set permissions admin >> user >> access
2. Fill in preferences at admin >> user >> autoassignrole