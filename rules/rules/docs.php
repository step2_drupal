<?php
// $Id: docs.php,v 1.1.2.3 2009/04/19 15:03:44 fago Exp $


/**
 * @file
 * This file contains no working PHP code; it exists to provide additional documentation
 * for doxygen as well as to document hooks in the standard Drupal manner.
 */

/**
 * @defgroup rules Rules module integration
 * @{
 *
 */

/**
 * This hook should be placed in MODULENAME.rules.inc.
 */
function hook_rules_action_info() {
  // example code here
}

/**
 * This hook should be placed in MODULENAME.rules.inc.
 */
function hook_rules_condition_info() {
  // example code here
}

/**
 * @}
 */

