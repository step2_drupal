// $Id: navigate_custom.js,v 1.1 2008/10/27 17:51:04 stompeers Exp $

$(document).ready(function(){
  navigate_custom_load();
});

/**
 * Load on document load
 */
function navigate_custom_load(item) {
  //item = navigate_item_default(item);
}


/**
 * Save the content and reload it
 */
function navigate_custom_save(wid) {
  var callback = 'navigate_custom_msg';
  
  $('.navigate-custom-output-' + wid).animate({'opacity': 0.4});
  
  var query_array = new Array();
  query_array['module'] = 'navigate_custom';
  query_array['action'] = 'save';
  query_array['content'] = $('#navigate-content-input_' + wid).val();
  navigate_process(wid, query_array, callback);
}


/**
 * Callback for positive result from ajax query
 */
function navigate_custom_msg(msg, wid) {
  $('.navigate-custom-output-' + wid).html(msg);
  $('.navigate-custom-output-' + wid).animate({'opacity': 1}, function() {
    navigate_cache_save();
  });
}
