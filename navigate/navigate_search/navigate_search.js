// $Id: navigate_search.js,v 1.2 2008/10/26 00:45:08 stompeers Exp $


/**
 * Grab search variables and send to be processed
 */
function navigate_search_process(wid) {
  var callback = 'navigate_search_process_msg';
  
  var query_array = new Array();
  query_array['module'] = 'navigate_search';
  query_array['action'] = 'search';
  query_array['phrase'] = $('#navigate-search-phrase_' + wid).val();
  $('.navigate-search-results-' + wid).slideUp('slow', function () {
    
    // Run through navigate_process so the API can handle the query string
    navigate_process(wid, query_array, callback);
  });
}


/**
 * Callback for positive result from ajax query
 */
function navigate_search_process_msg(msg, wid) {
  $('.navigate-search-results-' + wid).html(msg);
  $('.navigate-search-results-' + wid).slideDown('slow', function() {
    $('.navigate-search-results-' + wid).removeAttr('style'); // slideUp and slideDown add some styles we need to get rid of before saving
    navigate_cache_save();
  });
  
}
