// $Id: navigate_favorites.js,v 1.3 2008/11/04 19:56:20 stompeers Exp $

$(document).ready(function(){
  navigate_favorites_load();
});

/**
 * Load on document load
 */
function navigate_favorites_load(item) {
  item = navigate_item_default(item);
  $(item + ".navigate-favorites-list").sortable({
    cursor: "move",
    revert:true,
    distance: 4,
    helper:"clone",
    stop:function(e, ui) {
      var wid = $(this).parents('.navigate-widget-outer').children('.wid').val();
      var query_array = new Array();
      query_array['module'] = 'navigate_favorites';
      query_array['action'] = 'sort';
      query_array[''] = $(this).sortable('serialize');
      navigate_process(wid, query_array, 'navigate_cache_save');
    }
  });
  
  // Load delete links
  $(item + '.navigate-favorites-delete').click(function() {
    var callback = 'navigate_favorites_msg';
    var wid = $(this).parents('.navigate-widget-outer').children('.wid').val();
    $('.navigate-favorites-list-' + wid).animate({'opacity': 0.4});
    var query_array = new Array();
    query_array['module'] = 'navigate_favorites';
    query_array['action'] = 'remove';
    query_array['fav_id'] = $(this).children('.navigate-favorites-id').val();
    navigate_process(wid, query_array, callback);
  });
  
  // Bind click to delete buttons
  navigate_favorites_bind_delete(item);
}


/**
 * Bind events to delete button
 */
function navigate_favorites_bind_delete(item) {
  $(item + '.navigate-favorites-delete').click(function() {
    var callback = 'navigate_favorites_msg';
    var wid = $(this).parents('.navigate-widget-outer').children('.wid').val();
    $('.navigate-favorites-list-' + wid).animate({'opacity': 0.4});
    var query_array = new Array();
    query_array['module'] = 'navigate_favorites';
    query_array['action'] = 'remove';
    query_array['fav_id'] = $(this).children('.navigate-favorites-id').val();
    navigate_process(wid, query_array, callback);
    
  });
  
  // Show delete links when hovering
  $(item + '.navigate-favorites-link-outer').hover(
    function() {
      $(this).children('.navigate-favorites-delete').show();
    },
    function() {
      $(this).children('.navigate-favorites-delete').hide();
    }
  );
}


/**
 * Process adding of a favorite
 */
function navigate_favorites_add(wid) {
  var callback = 'navigate_favorites_msg';
  
  $('.navigate-favorites-list-' + wid).animate({'opacity': 0.4});
  
  var query_array = new Array();
  query_array['module'] = 'navigate_favorites';
  query_array['action'] = 'add';
  query_array['name'] = $('#navigate-favorite-name_' + wid).val();
  navigate_process(wid, query_array, callback);
}


/**
 * Callback for positive result from ajax query
 */
function navigate_favorites_msg(msg, wid) {
  $('.navigate-favorites-list-' + wid).html(msg);
  $('.navigate-favorites-list-' + wid).animate({'opacity': 1}, function() {
    navigate_favorites_bind_delete('.navigate-favorites-list-' + wid + ' ');
    $('.navigate-favorites-delete').hide();
    navigate_cache_save();
  });
}
