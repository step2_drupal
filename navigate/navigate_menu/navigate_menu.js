// $Id: navigate_menu.js,v 1.1 2008/10/27 17:51:05 stompeers Exp $

$(document).ready(function(){
  navigate_menu_load();
});

/**
 * Load on document load
 */
function navigate_menu_load(item) {
  item = navigate_item_default(item);
  
  $(item + ".navigate-menu-list").each(function() {
    var wid = $(this).parents('.navigate-widget-outer').children('.wid').val();
    $(this).treeview({
      animated: "fast",
      collapsed: true,
      persist: "cookie",
      cookieId: "navigationtree_" + wid
    });
  });
}


/**
 * Save the content and reload it
 */
function navigate_menu_load_menu(wid) {
  var callback = 'navigate_menu_msg';
  
  $('.navigate-menu-output-' + wid).animate({'opacity': 0.4});
  
  var query_array = new Array();
  query_array['module'] = 'navigate_menu';
  query_array['action'] = 'load';
  query_array['mid'] = $('.navigate-menu-output-' + wid).find('select').val();
  navigate_process(wid, query_array, callback);
}


/**
 * Callback for positive result from ajax query
 */
function navigate_menu_msg(msg, wid) {
  $('.navigate-menu-output-' + wid).html(msg);
  navigate_menu_load(' .navigate-menu-output-' + wid);
  $('.navigate-menu-output-' + wid).animate({'opacity': 1}, function() {
    navigate_cache_save();
  });
}
