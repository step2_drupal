// $Id: navigate.js,v 1.5.4.2 2008/11/06 01:59:12 stompeers Exp $

var navigate_loader_count = 0;

$(document).ready(function(){
  navigate_load();
});

/**
 * This is run on document load ready and when any widget is added. Widget class is passed via 'item'
 */
function navigate_load_bind_widgets(item) {
  
  // Set item if it's not set
  item = navigate_item_default(item);
  
  // Bind events to on/off buttons
  navigate_bind_toggle_buttons(item);
  
  // Bind events to callback buttons
  navigate_bind_callback_buttons(item);
  
  // Bind events to text inputs
  navigate_bind_text_inputs(item);
  
  // Bind events to close button
  navigate_bind_widget_close();
  
  // Bind events to textarea
  navigate_bind_textareas();
  
   // Bind event to widget settings button
  $(item + ' .navigate-widget-settings-button').click(function() {
    $(this).parent().find('.navigate-widget-settings-outer').slideToggle('slow', function() { navigate_cache_save();});
  });
  
  // Add select all on text inputs
  $(item + " .navigate-select-all").focus(function () {
    $(this).select();
  });
  
  // Bind doubleclick for title change
  navigate_bind_title_change(item);
  
  // Add tooltips
  navigate_add_tooltips();
  
}


/**
 * Bind doubleclick event to title to change and save it
 */
function navigate_bind_title_change(item) {
  // Add double-click to widget title to change it
  $(item + ".navigate-widget-title").dblclick(function() {
    var parent = $(this).parent();
    var wid = $(parent).attr('rel');
    $(parent).html('<input type="text" class="navigate-title-input" value="' + $(this).text() + '" />');
    $(parent).children().select();
    $(parent).children().bind('keyup', function(e) {
      // If pressing enter
      if (e.which == 13 || e.keyCode == 13) {
        if ($(parent).children().val() == '') {
          alert('Please enter some value for the title');
        } else {
          navigate_variable_set('widget_title', $(parent).children().val(), wid);
          $(parent).html('<div class="navigate-widget-title">' + $(parent).children().val() + '</div>');
          var parent_id = $(parent).attr('id');
          navigate_bind_title_change('#' + parent_id + ' ');
          navigate_cache_save();
        }
      }
    });
  });
}


/**
 * Set default item to bind events to
 *
 * If 'item' is not set, this will return .navigate, the class of the div containing the entire navigation
 */
function navigate_item_default(item) {
  item = typeof(item) != 'undefined' ? item : '.navigate ';
  return item + ' ';
}


/**
 * Bind event for clicking a widgets close button
 */
function navigate_bind_widget_close() {
  $('.navigate-widget-close').click(function() {
    var wid = $(this).attr('rel');
    navigate_queue_add();
    $.ajax({
        url: navigate_process_url(),
        type: 'POST',
        data: 'action=widget_delete&wid=' + wid,
        error: function() {
        },
        success: function(msg) {
          $('#navigate-widget-outer-' + wid).slideUp('slow', function() {
            $('#navigate-widget-outer-' + wid).remove();
            navigate_queue_subtract();
            navigate_cache_save();
          });
        }
      });
  });
}


/**
 * Binds events and various errata after page is loaded
 */
function navigate_load() {
  
  // Bind click events to buttons and inputs to all widgets
  navigate_load_bind_widgets();
  
  // Bind click event to widget list (if visible)
  navigate_bind_add_widget_list();
  
  // Run pngFix for IE6
  $('.navigate').pngFix();
  
   // Make widgets sortable using their title as the handle
  $(".navigate-all-widgets").sortable({
      cursor: "move",
      distance: 10,
      handle: ".navigate-widget-title",
      revert:true,
      stop:function(e, ui) {
        navigate_queue_add();
        $.ajax({
          url: navigate_process_url(),
          type: 'POST',
          data: 'action=widget_sort&' + $(".navigate-all-widgets").sortable('serialize'),
          error: function() {
          },
          success: function(msg) {
            navigate_queue_subtract();
            navigate_cache_save();
          }
        });
      }
    });

   // Bind click event to setting button to show close buttons and new widgets list
  $('.navigate-launch-settings').click(function() {
    navigate_queue_add();
    $.ajax({
      url: navigate_process_url(),
      type: 'POST',
      data: 'action=widget_list',
      error: function() {
      },
      success: function(msg) {
        navigate_queue_subtract();
        $('.navigate-add-widgets').html(msg);
        $('.navigate-add-widgets').pngFix();
        $('.navigate-add-widgets').slideToggle('slow', function() {
          $('.navigate-widget-close').toggle();
          navigate_cache_save();
        });
        navigate_bind_add_widget_list();
      }
    });
  });
  
  // Add tooltips
  navigate_add_tooltips()
  
  // Bind event to text input to save the value of the input.
  // Since the .val() and .attr() functions don't change the actual html of an input,
  // we have to add a hidden input with the value of the text field, and then replace the text field with the value
  // when the page is loaded.
  $(".navigate-text-input").each(function(){
    var rel = $(this).attr('id');
    var outer = $(this);
    if ($("input[rel='" + rel + "']").val() != 'undefined') {
      $(this).val($("input[rel='" + rel + "']").val());
    }
  });
  
  // Clear user cache when button is clicked
  $('.navigate-clear-cache').click(function() {
    $.ajax({
        url: navigate_process_url(),
        type: 'POST',
        data: 'action=clear_user_cache',
        error: function() {
        },
        success: function(msg) {
          document.location = document.location;
        }
      });
  });
  

  // Run pngFix on switch button
  $('#navigate-switch-outer').pngFix();
 
  // Add select all on text inputs with select all specified
  $(".navigate-select-all").focus(function () {
    $(this).select();
  });
  
    // Bind events to switch button
  $("#navigate-switch").click(function () {
    // Close
    if (navigate_on == 1) {
      navigate_on = 0;
      navigate_variable_set('on', '0');
      $(".navigate").stop();
      $("body").animate({ paddingLeft: "0px"}, "slow");
      $(".navigate").animate({ marginLeft: "-550px"}, "slow");
      $("#navigate-switch").animate({ marginLeft: "-230px", marginTop: "-30px"}, "slow");
      $(".navigate-loading").animate({ marginLeft: "-550px"}, "slow");
    } else {
      // Open
      navigate_on = 1;
      navigate_variable_set('on', '1');
      $("body").stop();
      $("body").animate({ paddingLeft: "210px"}, "slow");
      $(".navigate").animate({ marginLeft: "-195px"}, "slow");
      $("#navigate-switch").animate({ marginLeft: "-195px", marginTop: "0px"}, "slow");
      $(".navigate-loading").animate({ marginLeft: "-30px"}, "slow");
    }
    
  });
  
  
  // Bind click event to acklowledgement link 
  $(".navigate-acknowledgement-switch").click(function () {
    $('.navigate-acknowledgement').slideToggle('slow');
  });
  
  // Bind doubleclick event to title
  $('.navigate-title').dblclick(function() {
    $('.navigate-shorten').slideToggle('fast', function() {
      navigate_cache_save();
    });
  });
}


/**
 * Bind click actions to on/off buttons.
 */
function navigate_bind_toggle_buttons(item) {
  item = navigate_item_default(item);
  
  $(item + ' .navigate-button-outer').each(function() {
    var wid = navigate_widget_id(this);
    var widget = this;
    $(this).children().click(function() {
      
      // Grab all variables from hidden inputs
      var on = $(widget).find('.on').val();
      var off = $(widget).find('.off').val();
      var name = $(widget).find('.name').val();
      var required = $(widget).find('.required').val();
      var callback = $(widget).find('.callback').val();
      var val;
      
      // If it's a group
      if ($(widget).find('.group').length > 0) {
        var group = $(widget).find('.group').val();
        if ($(this).hasClass('navigate-button-on')) {
          if (required == 1) {
            return;
          }
          $(this).parents('.navigate-widget').find('.' + group).each(function(child) {
            $(this).removeClass('navigate-button-on');
          });
          navigate_variable_set(name, '', wid);
          navigate_cache_save();
          return;
        }
        $(this).parents('.navigate-widget-outer').find('.' + group).each(function(child) {
          $(this).removeClass('navigate-button-on');
        });
        val = on;
        $(this).addClass('navigate-button-on');
      
      // If not a group
      } else {
        // If it's just a callback
        if (on != '') {
          if ($(this).hasClass('navigate-button-on')) {
            val = off;
            $(this).removeClass('navigate-button-on');
          } else {
            val = on;
            $(this).addClass('navigate-button-on');
          }
        }
      }
      if (callback != '' && callback != undefined) {
        window[callback](wid);
      }
      navigate_variable_set(name, val, wid);
      navigate_cache_save();
    });
  });
}


/**
 * Bind callback click actions to buttons.
 */
function navigate_bind_callback_buttons(item) {
  item = navigate_item_default(item);
  $(item + ' .navigate-click-outer').each(function() {
    var wid = navigate_widget_id(this);
    var widget = this;
    $(this).children().click(function() {
      var callback = $(widget).children('.callback').val();
      if (callback != '' && callback != undefined) {
        window[callback](wid);
      }
    });
  });
}


/**
 * Bind text input events, including value saving and callback on pressing enter
 */
function navigate_bind_text_inputs(item) {
  item = navigate_item_default(item);
  var search_timeout = undefined;
  $(item + ' .navigate-text-input-outer').each(function() {
    
    var wid = navigate_widget_id(this);
    var widget = this;
    var callback = $(widget).children('.callback').val();
    var name = $(widget).children('.name').val();
    var text_input = this;
    
    $(this).find('input').bind('keyup', function(e) {
      var input_name = $(text_input).children('div').children('input').attr('name');
      var id = $(text_input).children('div').children('input').attr('id');
      var holder_id = name + wid + '_holder';
      // If pressing enter
      if (e.which == 13 || e.keyCode == 13) {
        if (callback != '' && callback != undefined) {
          window[callback](wid);
          if ($(this).hasClass('navigate-clear')) {
            var val = '';
            $(this).val('');
            $('.' + holder_id).remove();
            var new_input = '<div  class="' + holder_id + '"><input rel="' + id + '" type="hidden" name="' + input_name + '" value="' + val + '" /></div>';
            $(text_input).parent().append(new_input);          
            //navigate_variable_set(name, '', wid);
          }
        }
      } else {
        if(search_timeout != undefined) {
                clearTimeout(search_timeout);
        }
        var $this = this;
        
        // Set timeout to save input text when typing is paused
        search_timeout = setTimeout(function() {
          search_timeout = undefined;
          var val = $(text_input).children('div').children('input').val();
          $('.' + holder_id).remove();
          var new_input = '<div  class="' + holder_id + '"><input rel="' + id + '" type="hidden" name="' + input_name + '" value="' + val + '" /></div>';
          $(text_input).parent().append(new_input);          
          navigate_variable_set(name, val, wid);
          navigate_cache_save();
        }, 500);
      }
    });

  });
}


/**
 * Bind textarea save event, including value saving and callback on pressing enter
 */
function navigate_bind_textareas(item) {
  item = navigate_item_default(item);
  $(item + ' .navigate-textarea-outer').each(function() {
    var wid = navigate_widget_id(this);
    var widget = this;
    var callback = $(widget).children('.callback').val();
    var name = $(widget).children('.name').val();
    var text_input = this;
    $(this).find('.navigate-submit').click(function() {
      var val = $(widget).find('.navigate-textarea').val();
      navigate_variable_set(name, val, wid);
      $(widget).find('.navigate-textarea').html(val);
      if (callback != '' && callback != undefined) {
        window[callback](wid);
      }
    });

  });
}


/**
 * Add tooltips
 */
function navigate_add_tooltips() {
  // Add tooltips
  $('.navigate-tip').tooltip({
    bodyHandler: function() {
      return $($(this).attr("rel")).html();
    },
    track: true,
    fixPNG: true,
    fade: 150,
    delay:750
  });
}


/**
 * Set a variable
 */
function navigate_variable_set(name, val, wid) {
  
  // Save the variable in a database
  navigate_queue_add();
   $.ajax({
      url: $("#navigate_process_url").val(),
      type: 'POST',
      data: 'action=variable_save&name=' + name + '&wid=' + wid + '&value=' + encodeURIComponent(val),
      error: function() {
      },
      success: function(msg) {
        navigate_queue_subtract();
      }
  });
}

/**
 * Add count to loader queue
 */
function navigate_queue_add() {
  if (navigate_loader_count < 1) {
    $(".navigate-loading").show();
  }
  navigate_loader_count++;
}

/**
 * Remove count from loader
 */
function navigate_queue_subtract() {
  navigate_loader_count--;
  if (navigate_loader_count < 1) {
    $(".navigate-loading").slideUp();
  }
}

/**
 * Save html to cache
 */
function navigate_cache_save() {
  
  // Refresh tooltips, added here for convenience so widgets don't have to call it on laoding
  navigate_add_tooltips();
  
  navigate_queue_add();
  $.ajax({
      url: $("#navigate_process_url").val(),
      type: 'POST',
      data: 'action=cache_save&content=' + encodeURIComponent($('.navigate').html()),
      error: function() {
      },
      success: function(msg) {
        navigate_queue_subtract();
      }
  });
}


/**
 * Grab the wid of a widget from inside the widget
 */
function navigate_widget_id(item) {
  return $(item).parents('.navigate-widget-outer').children('.wid').val();
}


/**
 * Get the location of the current URL
 */
function navigate_process_url() {
  return $("#navigate_process_url").val();
}


/**
 * Send request to navigate processing function
 *
 * This is used to run widget callbacks through, so we can sanitize query string data.
 */
function navigate_process(wid, query_array, callback) {
  var query_string = '';
  for(var i in query_array) {
    if (i != '') {
      query_string += i + '=';
    }
    query_string += query_array[i] + '&';
  }
  query_string += '&wid=' + wid + '&return=' + $('#navigate_q').val();
  navigate_queue_add();
  $.ajax({
      url: navigate_process_url(),
      type: 'POST',
      data: query_string,
      error: function() {
      },
      success: function(msg) {
        navigate_queue_subtract();
        if (callback != undefined) {
          window[callback](msg, wid);
        }
      }
  });
}


/**
 * Binds click event to list of widgets
 */
function navigate_bind_add_widget_list() {
  $('.navigate-widget-list-item').click(function() {
    $list_item = $(this);
    navigate_queue_add();
    $.ajax({
      url: navigate_process_url(),
      type: 'POST',
      dataType: 'html',
      data: 'action=add_widget&module=' + $(this).attr('name') + '&type=' + $(this).attr('rel'),
      error: function() {
      },
      success: function(msg) {
        navigate_queue_subtract();
        $('.navigate-all-widgets').append(msg);
        var widget = '#' + $(msg).parent().children('.navigate-widget-outer').attr('id');
        navigate_load_bind_widgets(widget);
        funct = $list_item.find('.callback').val();
        if (funct != '' && funct != undefined) {
          window[funct](widget);
        }
        navigate_cache_save();
      }
    });
  });
}
